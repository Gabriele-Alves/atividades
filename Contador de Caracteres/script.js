$(document).ready(function() {

   // const button = $("#btn");
   // const input = $("#texto").val();
   // ("aeiou".indexOf(char) !== -1)

    $("#btn").on("click", function() {
        const input = $("#texto").val();
        let t = 0; //número de caracteres
        let v = 0; //número de vogais
        let c = 0; //número de consoantes
        let e = 0; //número de espaços

        for (let i = 0; i < input.length; i++) {
            const char = input[i].toLowerCase();
            if (char == " ") {
                e++;
            } else if (/[aeiou]/.test(char)) {
                v++;
            } else if (/[a-z]/.test(char)){  // regular expression
                c++;
            }
            t++;
        };

        $("#resultado").html("A frase contém: <br>"+ t + " caracteres,<br>" 
        + v + " vogais,<br>" + c + " consoantes,<br>" + e + " espaços.");
    });
});