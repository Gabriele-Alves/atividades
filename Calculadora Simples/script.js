$(document).ready(function() {
    const button = $("#calcular");

    $(button).on("click", function() {
        const input1 = $("#num1").val();
        const number1 = parseInt(input1);
        const input2 = $("#num2").val();
        const number2 = parseInt(input2);

        const operacao = $("#operacao").val();

        switch (operacao) {
            case "soma":
                const soma = number1 + number2;
                $("#resultado").text("= " + soma);
                break;
            case "subtracao":
                const subtracao = number1 - number2;
                $("#resultado").text("= " + subtracao);
                break;
            case "multiplicacao":
                const multiplicacao = number1 * number2;
                $("#resultado").text("= " + multiplicacao);
                break;
            case "divisao":
                const divisao = number1 / number2;
                $("#resultado").text("= " + divisao);
                break;
        }

    });

});