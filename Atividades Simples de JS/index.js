
console.log("Practice 1");

let i = '#';
while(i.length<8){
  console.log(i);
  i += '#';
}

console.log("Practice 2");

let tamanho = Number(prompt("Qual o tamanho do tabuleiro?"));  // input -> integer
let tabuleiro = "";                                            // string vazia
for (i = 0; i < tamanho; i++){                                 // linhas
  for (j = 0; j < tamanho; j++){                               // colunas
    (i + j) % 2 == 0 ? tabuleiro += "#" : tabuleiro += " ";    // # = par e " " = ímpar
  }
  tabuleiro += "\n";                                           // próxima linha
}
console.log(tabuleiro);

console.log("Practice 3");

function soma(array){
  let separado = array.split(" ");
  let i = 0;
  let resultado = 0;
  while (separado[i]){
    resultado += parseInt(separado[i]);
    i++;
  }
  return resultado;
}
let array = prompt("Digite uma sequência de números separados por espaço");
console.log("A soma da sequência de números é:", soma(array));

console.log("Practice 4");

let array1 = prompt("Digite uma sequência de numeros separados por espaço");
let newArray = [...array1].slice().reverse();
console.log("Invetendo a sequência:", newArray);

console.log("Practice 5");

let string = prompt("Digite uma palavra");
let letra = prompt("Digite uma letra");
function contraLetras(string, letra){
  let i = 0;
  let resultado = 0;
  while (string[i]){
    if (string[i] == letra){
      resultado++;
    }
    i++;
  }
  return resultado;
}
console.log("A letra", letra, "aparece", contraLetras(string, letra), "vezes na palavra", string);

console.log("Practice 6");

let frase = prompt("Digite uma frase");
let palavra = prompt("Digite uma palavra");

function procuraPalavra(frase, palavra){
  let i = 0;
  let posicao = 0;
  while (frase[i]){
    if (frase[i] == palavra[0]){
      if (frase.slice(i, i+palavra.length) == palavra){
        posicao = i;
        break;
      }
    }
    i++;
  }
  return posicao;
}
console.log("A palavra", palavra, "começa na posição", procuraPalavra(frase, palavra), "da frase", frase);