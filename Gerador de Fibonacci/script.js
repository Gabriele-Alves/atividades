function fibonacci(n) {
    if (n <= 1) {
        return n;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

function gerar() {
    const $numero = $("#numero");
    const $resultado = $("#resultado");
    const n = parseInt($numero.val(), 10);
    $resultado.empty();
    const minFontSize = 12;
    const maxFontSize = 36;
    const fontSizeDecrement = (maxFontSize - minFontSize) / n;
    let currentFontSize = maxFontSize;

    const fibNumbers = Array.from({ length: n }, (_, i) => fibonacci(i));
    const spans = fibNumbers.map(num => {
        const $span = $(`<span class="fib-number">${num}</span>`);
        $span.css({
            "font-size": `${currentFontSize}px`,
            "color": `#${Math.floor(Math.random() * 16777215).toString(16)}`
        });
        currentFontSize -= fontSizeDecrement;
        return $span;
    });

    $resultado.append(spans);
}

$(document).ready(function() {
    $("#btn").on("click", function() {
        gerar();
    });
});