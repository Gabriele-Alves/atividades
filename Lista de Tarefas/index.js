var uncompleted=[];
var completed=[];

function desenha_lista(uncompleted){

    var n = uncompleted.length;

    $("#uncompleted").html("");
    for(var i=0; i<n; i++)
        $("#uncompleted").append($("<li></li>").html(uncompleted[i]));

    return(uncompleted);
}


$(document).ready(function(ev){

    $("#list-form").on("submit",function(ev){
        ev.preventDefault();
        var item;

        uncompleted.push($("#item").val());
        $("#item").val("");

        desenha_lista(uncompleted);

        // Impede que o formulario seja submetido e pagina recarregue.
        return(false);
    });

    // Add a click event to all li elements in the "uncompleted" and "completed" lists.
    $("#uncompleted, #completed").on("click", "li", function(ev) {
        var li = $(this);
        var parentList = li.parent();
        var text = li.text();

        if (parentList.attr("id") === "uncompleted") {
            completed.push(text);
            uncompleted.splice(uncompleted.indexOf(text), 1);
            li.detach().appendTo("#completed");
        }
        else {
            uncompleted.push(text);
            completed.splice(completed.indexOf(text), 1);
            li.detach().appendTo("#uncompleted");
        }
    })

});