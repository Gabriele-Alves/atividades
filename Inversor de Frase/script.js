$(document).ready(function() {
    $("#btn").on("click", function() {
        const texto = $("#texto").val();
        const reversedText = texto.split("").reverse().join(""); // split() separa a string em um array, reverse() inverte o array e join() junta o array em uma string
        $("#resultado").html(reversedText);
    });
});

/* O QUE FAZER:
    - Converter a string em um array
    - Inverter o array
    - Converter o array em uma string
*/